#if 0
#include<iostream>
using namespace std;
//模板方法类，实现延迟到子类
//但父类提前写好一个方法

class abstractDrink {
public:
	virtual void boilWater() = 0;
	virtual void brew() = 0;
	virtual void pourInCup() = 0;
	virtual void addSomething() = 0;

	void make() {
		boilWater();
		brew();
		pourInCup();
		addSomething();
	}
};

class Coffee:public abstractDrink{
public:
	void boilWater() override {
		cout << "煮开水" << endl;
	}
	void brew() override {
		cout << "冲咖啡" << endl;
	}
	void pourInCup() override {
		cout << "倒入杯中" << endl;
	}
	void addSomething() override {
		cout << "加奶" << endl;
	}
};

class Tea :public abstractDrink {
public:
	void boilWater() override {
		cout << "煮开水" << endl;
	}
	void brew() override {
		cout << "泡茶" << endl;
	}
	void pourInCup() override {
		cout << "倒入杯中" << endl;
	}
	void addSomething() override {
		cout << "加糖" << endl;
	}
};

int main() {
	abstractDrink* drink = new Tea;
	drink->make();
	delete drink;
	drink = new Coffee;
	drink->make();
	delete drink;
}
#endif