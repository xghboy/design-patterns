#if 0
#include<iostream>
#include<queue>
#include<Windows.h>
using namespace std;
//命令模式，类似计网中的协议

//协议处理类
class protocalManager {
public:
	//增加金币
	void addMoney() {
		cout << "增加金币" << endl;
	}

	void addDiamond() {
		cout << "增加钻石" << endl;
	}

	void addEquipment() {
		cout << "穿装备" << endl;
	}

	void addLevel() {
		cout << "玩家升级" << endl;
	}
};

//命令接口
class abstractCommand {
public:
	virtual void handle() = 0;
};

class addMoneyCommend :public abstractCommand {
public:
	addMoneyCommend(protocalManager* p) :user(p) {}
	void handle() override{
		user->addMoney();
	}
private:
	protocalManager* user;
};

class addDiamondCommend :public abstractCommand {
public:
	addDiamondCommend(protocalManager* p) :user(p) {}
	void handle() override {
		user->addDiamond();
	}
private:
	protocalManager* user;
};

class addEquipmentCommend :public abstractCommand {
public:
	addEquipmentCommend(protocalManager* p) :user(p) {}
	void handle() override {
		user->addEquipment();
	}
private:
	protocalManager* user;
};

class addLevelCommend :public abstractCommand {
public:
	addLevelCommend(protocalManager* p) :user(p) {}
	void handle() override {
		user->addLevel();
	}
private:
	protocalManager* user;
};

class Server {
public:
	void addRequest(abstractCommand* request) {
		commands.push(request);
	}

	void startHandle() {
		while (!commands.empty()) {
			Sleep(2000);
			abstractCommand* request = commands.front();
			request->handle();
			commands.pop();
			delete request;
		}
	}
private:
	queue<abstractCommand*> commands;
};

int main() {
	protocalManager* protocol = new protocalManager;
	abstractCommand* money = new addMoneyCommend(protocol);
	abstractCommand* diamond = new addDiamondCommend(protocol);
	abstractCommand* level = new addLevelCommend(protocol);
	abstractCommand* equipment = new addEquipmentCommend(protocol);
	Server server;

	server.addRequest(money);
	server.addRequest(diamond);
	server.addRequest(level);
	server.addRequest(equipment);

	server.startHandle();

	delete protocol, money, diamond, level, equipment;
}
#endif