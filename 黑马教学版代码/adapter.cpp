#if 0
#include<iostream>
#include<vector>
#include<algorithm>
using namespace std;
//适配器模式，将一个类的接口转换成客户希望的另一个接口

struct myPrint {
	void operator() (int v1, int v2) {
		cout << v1 + v2 << endl;
	}
};

//定义目标接口
class Target {
public:
	virtual void operator()(int v) = 0;
};

//写适配器
class Adapter :public Target {
public:
	Adapter(int param):parament(param){}
	void operator()(int v) override {
		printThing(v, parament);
	}
private:
	myPrint printThing;
	int parament;
};

//myBind2nd
Adapter myBind2nd(int a) {
	return Adapter(a);
}

int main() {
	vector<int> v;
	for (int i = 0; i < 10; i++) v.push_back(i);
	for_each(v.begin(), v.end(), myBind2nd(10));
	//myBind2nd返回了一个带参数的适配器，还是一个Adapter
	//相当于直接调用伪函数Adapter
}
#endif