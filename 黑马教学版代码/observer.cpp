#if 0
#include<iostream>
#include<list>
using namespace std;
//观察者模式，一个类变化其他类也变化

class abstractHero {
public:
	virtual void update() = 0;
};

class Mafario :public abstractHero{
public:
	Mafario() {
		cout << "玛法里奥上场" << endl;
	}
	void update() override{
		cout << "玛法里奥待机" << endl;
	}
};

class Iridan :public abstractHero {
public:
	Iridan() {
		cout << "伊利丹上场" << endl;
	}
	void update() override {
		cout << "伊利丹待机" << endl;
	}
};

class Tarande :public abstractHero {
public:
	Tarande() {
		cout << "泰兰德上场" << endl;
	}
	void update() override {
		cout << "泰兰德待机" << endl;
	}
};

class abstractBoss {
public:
	virtual void addHero(abstractHero* hero) = 0;
	virtual void deleteHero(abstractHero* hero) = 0;
	virtual void notify() = 0;//重要，改变其他类状态
protected:
	list<abstractHero*> heroList;
};

class Arthas :public abstractBoss {
public:
	void addHero(abstractHero* hero) override{
		heroList.push_back(hero);
	}
	void deleteHero(abstractHero* hero) override{
		heroList.remove(hero);
	}
	void notify() override{
		for (auto hero : heroList) {
			hero->update();
		}
	}
};

int main() {
	abstractHero* mafario = new Mafario;
	abstractHero* tarande = new Tarande;
	abstractHero* iridan = new Iridan;

	abstractBoss* arthas = new Arthas;
	arthas->addHero(mafario);
	arthas->addHero(tarande);
	arthas->addHero(iridan);
	arthas->notify();
	arthas->deleteHero(mafario);
	arthas->notify();
	delete arthas, iridan, tarande, mafario;
}
#endif