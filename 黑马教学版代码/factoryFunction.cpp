#if 0
#include<iostream>
using namespace std;
//对简单工厂的工厂加一个基类
// 简单工厂+“开闭原则”=工厂方法模式
//类太多

class abstractFruit {
public:
	virtual void showName() = 0;
};

class Apple :public abstractFruit {
public:
	void showName() override {
		cout << "我是苹果！" << endl;
	}
};

class Banana :public abstractFruit {
public:
	void showName() override {
		cout << "我是香蕉！" << endl;
	}
};

class Coconut :public abstractFruit {
public:
	void showName() override {
		cout << "我是椰子！" << endl;
	}
};

class abstractFruitFactory {
public:
	virtual abstractFruit* createFruit() = 0;
};

class appleFactory :public abstractFruitFactory{
public:
	abstractFruit* createFruit() override {
		return new Apple;
	}
};

class bananaFactory :public abstractFruitFactory {
public:
	abstractFruit* createFruit() override {
		return new Banana;
	}
};

class coconutFactory :public abstractFruitFactory {
public:
	abstractFruit* createFruit() override {
		return new Coconut;
	}
};

void test() {
	abstractFruitFactory* factory = nullptr;
	abstractFruit* fruit = nullptr;

	factory = new appleFactory;
	fruit = factory->createFruit();
	fruit->showName();
	delete factory;
	delete fruit;

	factory = new bananaFactory;
	fruit = factory->createFruit();
	fruit->showName();
	delete factory;
	delete fruit;

	factory = new coconutFactory;
	fruit = factory->createFruit();
	fruit->showName();
	delete factory;
	delete fruit;
}

int main() {
	test();
}
#endif