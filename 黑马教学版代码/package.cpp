#include<iostream>
using namespace std;
//装饰模式，动态的给一个类增加功能

class abstractHero {
public:
	virtual void showStatus() = 0;
public:
	int HP;
	int MP;
	int attack;
	int defense;
};

class Arthas :public abstractHero{
public:
	Arthas() {
		HP = 0;
		MP = 0;
		attack = 0;
		defense = 0;
	}
	void showStatus() override {
		cout << "血量：" << HP << endl;
		cout << "蓝量：" << MP << endl;
		cout << "攻击力：" << attack << endl;
		cout << "防御力：" << defense << endl;
	}
};

class abstractEquipment :public abstractHero {
public:
	abstractEquipment(abstractHero* h):hero(h){}
	virtual void showStatus() = 0;
protected:
	abstractHero* hero;
};

class frostSorrow :public abstractEquipment {
public:
	frostSorrow(abstractHero* hero):abstractEquipment(hero){}
	void addValue() {
		cout << "阿尔萨斯拿起霜之哀伤" << endl;
		HP = hero->HP;
		MP = hero->MP;
		attack = hero->attack + 30;
		defense = hero->defense;
	}
	void showStatus() override{
		this->addValue();
		cout << "血量：" << HP << endl;
		cout << "蓝量：" << MP << endl;
		cout << "攻击力：" << attack << endl;
		cout << "防御力：" << defense << endl;
	}
};

class Horse :public abstractEquipment {
public:
	Horse(abstractHero* hero) :abstractEquipment(hero) {}
	void addValue() {
		cout << "阿尔萨斯骑上马" << endl;
		HP = hero->HP + 30;
		MP = hero->MP;
		attack = hero->attack;
		defense = hero->defense;
	}
	void showStatus() override {
		this->addValue();
		cout << "血量：" << HP << endl;
		cout << "蓝量：" << MP << endl;
		cout << "攻击力：" << attack << endl;
		cout << "防御力：" << defense << endl;
	}
};

int main() {
	abstractHero* arthas = new Arthas;
	arthas->showStatus();
	cout << "------------------------------" << endl;
	abstractHero* arthasWithEquipment = new frostSorrow(arthas);
	arthasWithEquipment->showStatus();
	cout << "------------------------------" << endl;
	abstractHero* arthasWithHorse = new Horse(arthasWithEquipment);
	arthasWithHorse->showStatus();

	delete arthas;
	delete arthasWithEquipment;
	delete arthasWithHorse;
}