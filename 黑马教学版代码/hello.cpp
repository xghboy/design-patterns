#if 0
#include<iostream>
using namespace std;
//开闭原则 对扩展开放，对修改关闭，增加功能只能增加代码
//单一职责原则：一个类只做一件事
//计算器，通过抽象实现方便的增加功能

class abstractCalculator {
public:
	virtual int getResult() = 0;
	abstractCalculator(int num1,int num2):a(num1),b(num2){}
	void inline setOperatorNumber(int num1, int num2) {
		a = num1; b = num2;
	}
protected:
	int a, b;
	int result = 0;//默认为0，这个程序完全可以忽略它，但如果需要不断运算则result需要保留，并作为新的a
};//一个抽象calculator类

class addCalculator:public abstractCalculator {
public:
	addCalculator(int num1,int num2):abstractCalculator(num1,num2){}
	int getResult() override{
		result = a + b;
		return result;
	}
};

class minusCalculator :public abstractCalculator {
public:
	minusCalculator(int num1, int num2) :abstractCalculator(num1, num2) {}
	int getResult() override {
		result = a - b;
		return result;
	}
};

class multiplyCalculator :public abstractCalculator {
public:
	multiplyCalculator(int num1, int num2) :abstractCalculator(num1, num2) {}
	int getResult() override {
		result = a * b;
		return result;
	}
};

class devideCalculator :public abstractCalculator {
public:
	devideCalculator(int num1, int num2) :abstractCalculator(num1, num2) {}
	int getResult() override {
		if (b == 0) {
			cout << "除数不合法！" << endl;
			return a;
		}
		result = a / b;//只保留商
		return result;
	}
};

class modCalculator :public abstractCalculator {
public:
	modCalculator(int num1, int num2) :abstractCalculator(num1, num2) {}
	int getResult() override {
		if (b == 0) {
			cout << "除数不合法！" << endl;
			return a;//随意返回一个值，只要不实际进行运算
			//或者可以写一个异常处理，这里懒得写了
		}
		result = a % b;
		return result;
	}
};

int main() {
	abstractCalculator* calculator = new addCalculator(1, 3);
	cout << calculator->getResult() << endl;
	calculator->setOperatorNumber(2, 6);
	cout << calculator->getResult() << endl;
	delete calculator;
	calculator = new devideCalculator(1, 0);
	calculator->getResult();
	delete calculator;
}
#endif