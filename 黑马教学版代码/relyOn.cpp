#if 0
#include<iostream>
using namespace std;
//依赖倒转原则（重要）:依赖抽象接口，不依赖于具体类

//银行工作人员
class bankWorker {
public:
	virtual void doBusiness() = 0;
};

class saveBankWorker :public bankWorker {
public:
	void doBusiness() override{
		cout << "存款" << endl;
	}
};

class payBankWorker :public bankWorker {
public:
	void doBusiness() override {
		cout << "支付" << endl;
	}
};

class transferBankWorker :public bankWorker {
public:
	void doBusiness() override {
		cout << "转账" << endl;
	}
};

//一系列中层模块
void clientDoBusiness(bankWorker* worker) {
	worker->doBusiness();
	delete worker;
}

//业务模块
void test() {
	clientDoBusiness(new saveBankWorker);
	clientDoBusiness(new payBankWorker);
	clientDoBusiness(new transferBankWorker);
}

//这里代码逻辑太简单，因此效果不明显
int main() {
	test();
}
#endif