#if 0
#include<iostream>
using namespace std;
//在两个类中如果可以使用继承或组合关系，优先使用组合关系
//人开车

class abstractCar {
public:
	virtual void run() = 0;
};

class Bettle : public abstractCar {
public:
	void run() override {
		cout << "甲壳虫正启动……" << endl;
	}
};

class Benz : public abstractCar {
public:
	void run() override {
		cout << "奔驰正启动……" << endl;
	}
};
//应该是虚继承，否则无法释放基类,但不知道为什么这里写成虚继承会出问题,可能因为基类本身就是虚基类

class personWithCar {
public:
	void setMyCar(abstractCar* car) {
		myCar = car;
	}
	void hangAround() {
		myCar->run();
		if (myCar != nullptr) {
			delete myCar;
		}
	}
private:
	abstractCar* myCar;
};
//这个案例比较简单，因此会觉得这种组合是寻常的
//但是许多复杂的案例中，很容易出现“人继承汽车”的情况
//当然不应该这样，应该使用代码中类似的组合形式

void test() {
	personWithCar* stormwine = new personWithCar;
	stormwine->setMyCar(new Benz);
	stormwine->hangAround();
	stormwine->setMyCar(new Bettle);
	stormwine->hangAround();
	delete stormwine;
}

int main() {
	test();
}
#endif