#if 0
#include<iostream>
#include<string>//不加也问题不大
#include<vector>
using namespace std;
//迪米特法则，又称为最少知识原则
//暴露出的信息越少越好
//买房子，通过一个中介层屏蔽掉具体房子信息

//一个楼盘的抽象类
class abstractBuilding {
public:
	abstractBuilding(string q):quality(q){}
	virtual void sale() = 0;
	inline string showQuality() {
		return quality;
	}
protected:
	string quality;//楼盘质量好坏
};

//天通苑
class tianTongYuan :public abstractBuilding {
public:
	tianTongYuan(string q):abstractBuilding(q){}
	tianTongYuan():abstractBuilding("高品质"){}
	void sale() override {
		cout << "天通苑 " + quality + " 被卖出!" << endl;
	}
};

//金辰园
class jinChenYuan :public abstractBuilding {
public:
	jinChenYuan(string q) :abstractBuilding(q) {}
	jinChenYuan() :abstractBuilding("中等品质") {}
	void sale() override {
		cout << "金辰园 " + quality + " 被卖出!" << endl;
	}
	
};

//中介类
class mediator {
public:
	//选择品质符合需求的楼盘
	abstractBuilding* selectQuality(const vector<abstractBuilding*>& buildings, string need) {
		//遍历所有楼盘，如果符合品质则售出
		for (abstractBuilding* building : buildings) {
			if (building->showQuality() == need) {
				building->sale();//应该放在中介类中，但函数比较简单，也就无所谓了
				return building;
			}
		}
	}//未考虑异常状况

	//对外售卖的接口（目前只考虑品质）
	abstractBuilding* findYourBuilding(string quality) {
		return selectQuality(vectorOfBuilding, quality);
	}

	mediator() {
		abstractBuilding* building = new tianTongYuan;
		vectorOfBuilding.push_back(building);
		building = new jinChenYuan;
		vectorOfBuilding.push_back(building);
	}//这里仅为了方便，赋值应该单独有一个函数

	~mediator() {
		for (abstractBuilding* building : vectorOfBuilding) {
			if (building != nullptr) {
				delete building;
			}
		}
	}
private:
	vector<abstractBuilding*> vectorOfBuilding;//注意一定要存指针，否则无法实例化对象，也就无法使用智能指针推断
};//该任务中无需对其进行复用，因此不做抽象（可写作单例）

int main() {
	mediator* zhongjie = new mediator;
	zhongjie->findYourBuilding("高品质");
	zhongjie->findYourBuilding("中等品质");
	delete zhongjie;
}
#endif